/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Practicas123;

import javax.swing.JOptionPane;

/**
 *
 * @author REDES
 */
public class jdlgRegistro extends javax.swing.JDialog {

    /**
     * Creates new form jdlgRecibo
     */
    public jdlgRegistro(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        //this.setSize(590,490);
        this.limpiar();
        this.deshabilitar();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtNumRecibo = new javax.swing.JTextField();
        txtNombre = new javax.swing.JTextField();
        txtDomicilio = new javax.swing.JTextField();
        btnNuevo = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        btnCalcular = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();
        btnCerrar = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        rdbDomicilio = new javax.swing.JRadioButton();
        rdbIndustrial = new javax.swing.JRadioButton();
        rdbComercial = new javax.swing.JRadioButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        txtTotal = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        txtSubtotal = new javax.swing.JTextField();
        txtImpuestos = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtCosto = new javax.swing.JTextField();
        txtConsumo = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(null);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("Num. Recibo:");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(30, 17, 100, 30);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setText("Nombre:");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(60, 70, 70, 30);

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel3.setText("Domicilio:");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(60, 110, 80, 30);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel4.setText("Tipo de Servicio:");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(10, 180, 120, 17);
        getContentPane().add(txtNumRecibo);
        txtNumRecibo.setBounds(150, 20, 120, 30);
        getContentPane().add(txtNombre);
        txtNombre.setBounds(150, 70, 180, 30);

        txtDomicilio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDomicilioActionPerformed(evt);
            }
        });
        getContentPane().add(txtDomicilio);
        txtDomicilio.setBounds(150, 110, 180, 30);

        btnNuevo.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        btnNuevo.setText("Nuevo");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });
        getContentPane().add(btnNuevo);
        btnNuevo.setBounds(440, 50, 99, 43);

        btnGuardar.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        btnGuardar.setText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });
        getContentPane().add(btnGuardar);
        btnGuardar.setBounds(440, 100, 99, 39);

        btnCalcular.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        btnCalcular.setText("Calcular");
        btnCalcular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCalcularActionPerformed(evt);
            }
        });
        getContentPane().add(btnCalcular);
        btnCalcular.setBounds(440, 150, 99, 39);

        btnCancelar.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        getContentPane().add(btnCancelar);
        btnCancelar.setBounds(50, 400, 91, 31);

        btnLimpiar.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnLimpiar.setText("Limpiar");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });
        getContentPane().add(btnLimpiar);
        btnLimpiar.setBounds(210, 400, 83, 31);

        btnCerrar.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnCerrar.setText("Cerrar");
        btnCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarActionPerformed(evt);
            }
        });
        getContentPane().add(btnCerrar);
        btnCerrar.setBounds(350, 400, 77, 31);

        jPanel1.setBackground(new java.awt.Color(153, 153, 0));
        jPanel1.setForeground(new java.awt.Color(153, 153, 0));

        rdbDomicilio.setBackground(new java.awt.Color(153, 153, 0));
        buttonGroup1.add(rdbDomicilio);
        rdbDomicilio.setSelected(true);
        rdbDomicilio.setText("Domicilio");
        rdbDomicilio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdbDomicilioActionPerformed(evt);
            }
        });

        rdbIndustrial.setBackground(new java.awt.Color(153, 153, 0));
        buttonGroup1.add(rdbIndustrial);
        rdbIndustrial.setText("Industrial");

        rdbComercial.setBackground(new java.awt.Color(153, 153, 0));
        buttonGroup1.add(rdbComercial);
        rdbComercial.setText("Comercial");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(rdbDomicilio)
                .addGap(18, 18, 18)
                .addComponent(rdbComercial)
                .addGap(18, 18, 18)
                .addComponent(rdbIndustrial)
                .addContainerGap(35, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(10, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rdbIndustrial)
                    .addComponent(rdbDomicilio)
                    .addComponent(rdbComercial))
                .addContainerGap())
        );

        getContentPane().add(jPanel1);
        jPanel1.setBounds(130, 170, 300, 40);

        jPanel2.setBackground(new java.awt.Color(255, 204, 204));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Calculos", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 14), new java.awt.Color(255, 255, 255))); // NOI18N
        jPanel2.setForeground(new java.awt.Color(255, 51, 255));
        jPanel2.setLayout(null);

        jLabel7.setText("Total a Pagar");
        jPanel2.add(jLabel7);
        jLabel7.setBounds(270, 60, 100, 30);

        txtTotal.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtTotal.setText("00");
        txtTotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTotalActionPerformed(evt);
            }
        });
        jPanel2.add(txtTotal);
        txtTotal.setBounds(360, 60, 76, 30);

        jLabel8.setText("Sub Total");
        jPanel2.add(jLabel8);
        jLabel8.setBounds(30, 20, 70, 30);

        jLabel9.setText("Impuestos");
        jPanel2.add(jLabel9);
        jLabel9.setBounds(20, 60, 100, 30);

        txtSubtotal.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtSubtotal.setText("00");
        txtSubtotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSubtotalActionPerformed(evt);
            }
        });
        jPanel2.add(txtSubtotal);
        txtSubtotal.setBounds(100, 20, 130, 30);

        txtImpuestos.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtImpuestos.setText("00");
        txtImpuestos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtImpuestosActionPerformed(evt);
            }
        });
        jPanel2.add(txtImpuestos);
        txtImpuestos.setBounds(100, 60, 130, 30);

        getContentPane().add(jPanel2);
        jPanel2.setBounds(20, 280, 470, 110);

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel5.setText("Kilowatts Consumidos:");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(260, 220, 140, 50);

        txtCosto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCostoActionPerformed(evt);
            }
        });
        getContentPane().add(txtCosto);
        txtCosto.setBounds(140, 230, 100, 30);

        txtConsumo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtConsumoActionPerformed(evt);
            }
        });
        getContentPane().add(txtConsumo);
        txtConsumo.setBounds(390, 230, 100, 30);

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel6.setText("Costo por Kilowatts:");
        getContentPane().add(jLabel6);
        jLabel6.setBounds(20, 230, 120, 30);

        jLabel10.setFont(new java.awt.Font("Tahoma", 2, 11)); // NOI18N
        jLabel10.setText("Fecha:");
        getContentPane().add(jLabel10);
        jLabel10.setBounds(320, 10, 33, 40);
        getContentPane().add(jDateChooser1);
        jDateChooser1.setBounds(360, 20, 81, 20);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtDomicilioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDomicilioActionPerformed
        // TODO add your handling code here:

    }//GEN-LAST:event_txtDomicilioActionPerformed

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        // TODO add your handling code here:
        this.txtNombre.setEnabled(true);
        this.txtNumRecibo.setEnabled(true);
        this.txtConsumo.setEnabled(true);
        this.txtDomicilio.setEnabled(true);
        
        this.rdbDomicilio.setSelected(true);
        

        this.btnGuardar.setEnabled(true);

    }//GEN-LAST:event_btnNuevoActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        if ( this.txtNombre.getText().matches("")
            || this.txtNumRecibo.getText().matches("")
                || this.txtConsumo.getText().matches("")
            || this.txtDomicilio.getText().matches("")){

            JOptionPane.showMessageDialog(this, "Falto capturar Informacion ");

            //JOptionPane.showMessageDialog(this, "Falto capturar informacion");

        }
        else{
            
            
            //aqui falta algo arriba 
            
            
            //agregar los datos del objeto 
            examen.setNombre(this.txtNombre.getText());
            examen.setNumRecibo(Integer.parseInt(this.txtNumRecibo.getText()));
            examen.setDomicilio(this.txtDomicilio.getText());
            examen.setKilowats(Float.parseFloat(this.txtConsumo.getText()));
            
            /// aquuiiii va algo massss 
            
            
            //float Consumo = this.rdbComercial.getSelectedIcon();
            //determinar el costo
            if(this.rdbDomicilio.isSelected())
                examen.setTipoServicio(1);
            if(this.rdbComercial.isSelected())
                examen.setTipoServicio(2);
            if(this.rdbIndustrial.isSelected())
                examen.setTipoServicio(3);
            
            //this.btnCalcular.e
            this.btnCalcular.setEnabled(true);
            //this.btnCalcular.setEnabled(true);
        }
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnCalcularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCalcularActionPerformed
     
        this.txtSubtotal.setText(String.valueOf(examen.calcularSubtotal()));
            this.txtImpuestos.setText(String.valueOf(examen.calcularImpuesto()));
            this.txtTotal.setText(String.valueOf(examen.calcularTotal()));
            
            
            
            
    }//GEN-LAST:event_btnCalcularActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        // TODO add your handling code here:
        this.limpiar();
        //this.deshabilitar();
        this.btnCalcular.setEnabled(false);
   
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
        // TODO add your handling code here:
        limpiar();
        this.btnCalcular.setEnabled(true);
    }//GEN-LAST:event_btnLimpiarActionPerformed

    private void btnCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarActionPerformed
        // TODO add your handling code here:
        int examen = JOptionPane.showConfirmDialog(this,"Desea Salir", "Examen", JOptionPane.YES_NO_OPTION);
        this.setSize(600, 600);
        
            if (examen == JOptionPane.YES_OPTION) this.dispose();
    }//GEN-LAST:event_btnCerrarActionPerformed

    private void rdbDomicilioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdbDomicilioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rdbDomicilioActionPerformed

    private void txtTotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTotalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTotalActionPerformed

    private void txtCostoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCostoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCostoActionPerformed

    private void txtConsumoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtConsumoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtConsumoActionPerformed

    private void txtSubtotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSubtotalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSubtotalActionPerformed

    private void txtImpuestosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtImpuestosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtImpuestosActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(jdlgRegistro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(jdlgRegistro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(jdlgRegistro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(jdlgRegistro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                jdlgRegistro dialog = new jdlgRegistro(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCalcular;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnCerrar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.ButtonGroup buttonGroup1;
    private com.toedter.calendar.JDateChooser jDateChooser1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JRadioButton rdbComercial;
    private javax.swing.JRadioButton rdbDomicilio;
    private javax.swing.JRadioButton rdbIndustrial;
    private javax.swing.JTextField txtConsumo;
    private javax.swing.JTextField txtCosto;
    private javax.swing.JTextField txtDomicilio;
    private javax.swing.JTextField txtImpuestos;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtNumRecibo;
    private javax.swing.JTextField txtSubtotal;
    private javax.swing.JTextField txtTotal;
    // End of variables declaration//GEN-END:variables

    MetodosExamen examen = new MetodosExamen();
    
    public void deshabilitar(){
        this.txtNumRecibo.setEnabled(false);
        this.txtNombre.setEnabled(false);
        this.txtDomicilio.setEnabled(false);
        this.txtCosto.setEnabled(false);
        this.txtConsumo.setEnabled(false);
        
        this.txtSubtotal.setEnabled(false);
        this.txtImpuestos.setEnabled(false);
        this.txtTotal.setEnabled(false);
        
        this.btnCalcular.setEnabled(false);
        this.btnGuardar.setEnabled(false);
    }
        public void limpiar(){
            this.txtNumRecibo.setText("");
            this.txtNombre.setText("");        
            this.txtDomicilio.setText("");
            this.txtCosto.setText("");
            this.txtConsumo.setText("");
            
            this.txtSubtotal.setText("");
            this.txtImpuestos.setText("");
            this.txtTotal.setText("");
            this.rdbDomicilio.setSelected(true);
            
            
            
            this.btnGuardar.setEnabled(false);}
}
