/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Practicas123;

/**
 *
 * @author REDES
 */
public class MetodosExamen {
    private int numRecibo;
            private String nombre;
            private String domicilio;
            private int tipoServicio;
            private float costo;
            private float kilowats;
            private String fecha;
            
            
        public MetodosExamen (){
            this.numRecibo = 0;
            this.nombre = "";
            this.domicilio = "";
            this.tipoServicio = 0;
            this.costo = 0.0f;
            this.kilowats = 0.0f;
            this.fecha = "";
        }

        public MetodosExamen (int numRecibo, String nombre, String domicilio, 
                int tipoServicio, float costo, float kilowats, String fecha){
                this.numRecibo = numRecibo;
                this.nombre = nombre;
                this.domicilio = domicilio;
                this.tipoServicio = tipoServicio;
                this.costo = costo;
                this.kilowats = kilowats;
                this.fecha = fecha;
        }
        public MetodosExamen (MetodosExamen otro){
                this.numRecibo = otro.numRecibo;
                this.nombre = otro.nombre;
                this.domicilio = otro.domicilio;
                this.tipoServicio = otro.tipoServicio;
                this.costo = otro.costo;
                this.kilowats = otro.kilowats;
                this.fecha = otro.fecha;
        }

    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getTipoServicio() {
        return tipoServicio;
    }

    public void setTipoServicio(int tipoServicio) {
        this.tipoServicio = tipoServicio;
    }

    public float getCosto() {
        return costo;
    }

    public void setCosto(float costo) {
        this.costo = costo;
    }

    public float getKilowats() {
        return kilowats;
    }

    public void setKilowats(float kilowats) {
        this.kilowats = kilowats;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    
    //METODOS DE COMPORTAMIENTO
    
    public float calcularSubtotal (){
       float calSub = 0.0f;
       
     if(tipoServicio == 1){
         calSub = this.kilowats * 2.00f;
     }
     
     if(tipoServicio == 2){
         calSub = this.kilowats * 3.00f;
     }
     
     if(tipoServicio == 3){
         calSub = this.kilowats * 5.00f;
     }
     return calSub;
}
    
    public float calcularImpuesto (){
        float calImpuesto = 0.0f;
        float impuesto = 0.16f;
      calImpuesto = this.calcularSubtotal() * impuesto;
      return calImpuesto;
    }
    
    public float calcularTotal (){
        float calTotal = 0.0f;
        
        calTotal = this.calcularSubtotal() + this.calcularImpuesto();
     return calTotal;  
    }
}