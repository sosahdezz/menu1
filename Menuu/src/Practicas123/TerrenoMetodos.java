/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Practicas123;

/**
 *
 * @author REDES
 */
public class TerrenoMetodos {
   
    private int numLote;
    private float ancho;
    private float largo;
    
    //CONSTRUCTORES
public TerrenoMetodos (){
    this.numLote = 0;
    this.ancho = 0.0f;
    this.largo = 0.0f;
}

    public TerrenoMetodos(int numLote, float ancho, float largo) {
        this.numLote = numLote;
        this.ancho = ancho;
        this.largo = largo;
    }
    public TerrenoMetodos(TerrenoMetodos otro) {
     this.numLote = otro.numLote;
     this.ancho = otro.ancho;
     this.largo = otro.largo;   
    }
    
    //METODOS GET AND SET
    public int getnumLote() {
        return numLote;
    }

    public void setnumLote(int numLote) {
        this.numLote = numLote;
    }

    public float getAncho() {
        return ancho;
    }

    public void setAncho(float ancho) {
        this.ancho = ancho;
    }

    public float getLargo() {
        return largo;
    }

    public void setLargo(float largo) {
        this.largo = largo;
    }
    
    //METODOS DE COMPORTAMIENTO
    
    public float calcularArea (){
        float caArea = 0.0f;
        caArea = (this.ancho * this.largo);
        return caArea;     
    }
    
    public float calcularPerimetro (){
        float caPerimetro = 0.0f;
        caPerimetro = (this.ancho  + this.largo + this.ancho + this.largo);
        return caPerimetro;
    }
}
